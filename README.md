# Kafka Streams SAQ Aggregation Demo

Shows how progress can be aggregated using Kafka Streams API.

Using Change-Data-Capture feeds from Cockroach tables we can
process changes as they arrive in near real-time.

We use stateful transformations to join using KTables. These
transformations are backed by a state store which is written
to disk, therefore it is advised to use a stateful set when
running this application on Kubernetes.

https://kafka-tutorials.confluent.io/foreign-key-joins/kstreams.html

We read the tables containing the progress records, the module
records, section records, subsection records and site module
records so that deleted entries and progress records related
to deleted entries can be filtered out of the end result.

A consuming application can read the stream of changes into a
database to support queries or push the events out in the form
of notifications.

