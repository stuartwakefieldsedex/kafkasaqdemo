package com.example.stream

import EntityCode
import ModuleCode
import SectionCode
import SiteCode
import SiteSAQModuleKey
import SiteSAQModuleProgress
import SubsectionCode
import org.apache.kafka.streams.test.TestRecord
import org.junit.jupiter.api.Test
import java.time.LocalDateTime
import kotlin.test.assertEquals

class ModuleProgressTest {

    @Test
    fun `aggregates the progress counts to the module level`() {
        testTopology().use { topology ->
            topology.advanceSaqSectionInputTopic().pipeValueList(
                listOf(
                    ModuleSectionChangeWrapper(
                        after = ModuleSectionChange(
                            moduleCode = ModuleCode("ZQM1"),
                            sectionCode = SectionCode("ZQS1"),
                            modifiedTime = LocalDateTime.parse("2020-01-01T00:00:00.000"),
                            deleted = false
                        )
                    ),
                    ModuleSectionChangeWrapper(
                        after = ModuleSectionChange(
                            moduleCode = ModuleCode("ZQM1"),
                            sectionCode = SectionCode("ZQS2"),
                            modifiedTime = LocalDateTime.parse("2020-01-01T00:00:00.000"),
                            deleted = false
                        )
                    )
                )
            )
            topology.advanceSaqProgressInputTopic().pipeValueList(
                listOf(
                    EntitySAQProgressChangeWrapper(
                        after = EntitySAQProgressChange(
                            entityCode = EntityCode("ZS1"),
                            sectionCode = SectionCode("ZQS1"),
                            subsectionCode = SubsectionCode("ZQU1"),
                            answerCount = 1,
                            visibleQuestionCount = 3,
                            modifiedTime = LocalDateTime.parse("2020-02-01T00:00:00.000"),
                            deleted = false
                        )
                    ),
                    EntitySAQProgressChangeWrapper(
                        after = EntitySAQProgressChange(
                            entityCode = EntityCode("ZS1"),
                            sectionCode = SectionCode("ZQS1"),
                            subsectionCode = SubsectionCode("ZQU2"),
                            answerCount = 2,
                            visibleQuestionCount = 5,
                            modifiedTime = LocalDateTime.parse("2020-02-01T00:00:00.000"),
                            deleted = false
                        )
                    ),
                    EntitySAQProgressChangeWrapper(
                        after = EntitySAQProgressChange(
                            entityCode = EntityCode("ZS1"),
                            sectionCode = SectionCode("ZQS2"),
                            subsectionCode = SubsectionCode("ZQU3"),
                            answerCount = 4,
                            visibleQuestionCount = 4,
                            modifiedTime = LocalDateTime.parse("2020-02-01T00:00:00.000"),
                            deleted = false
                        )
                    ),
                    EntitySAQProgressChangeWrapper(
                        after = EntitySAQProgressChange(
                            entityCode = EntityCode("ZS1"),
                            sectionCode = SectionCode("ZQS2"),
                            subsectionCode = SubsectionCode("ZQU4"),
                            answerCount = 2,
                            visibleQuestionCount = 3,
                            modifiedTime = LocalDateTime.parse("2020-02-01T00:00:00.000"),
                            deleted = false
                        )
                    )
                )
            )
            assertEquals(
                listOf(
                    SiteSAQModuleProgress(
                        siteCode = SiteCode("ZS1"),
                        moduleCode = ModuleCode("ZQM1"),
                        answerCount = 1,
                        visibleQuestionCount = 3,
                        modifiedTime = LocalDateTime.parse("2020-02-01T00:00:00.000")
                    ),
                    SiteSAQModuleProgress(
                        siteCode = SiteCode("ZS1"),
                        moduleCode = ModuleCode("ZQM1"),
                        answerCount = 3,
                        visibleQuestionCount = 8,
                        modifiedTime = LocalDateTime.parse("2020-02-01T00:00:00.000")
                    ),
                    SiteSAQModuleProgress(
                        siteCode = SiteCode("ZS1"),
                        moduleCode = ModuleCode("ZQM1"),
                        answerCount = 7,
                        visibleQuestionCount = 12,
                        modifiedTime = LocalDateTime.parse("2020-02-01T00:00:00.000")
                    ),
                    SiteSAQModuleProgress(
                        siteCode = SiteCode("ZS1"),
                        moduleCode = ModuleCode("ZQM1"),
                        answerCount = 9,
                        visibleQuestionCount = 15,
                        modifiedTime = LocalDateTime.parse("2020-02-01T00:00:00.000")
                    )
                ),
                topology.moduleProgressOutputTopic().readRecordsToList().map { it.value }
            )
        }
    }

    @Test
    fun `calculates the site module progress percentages`() {
        testTopology().use { topology ->
            val sections = listOf(
                changefeed(section(
                    moduleCode = ModuleCode("ZQM1"),
                    sectionCode = SectionCode("ZQS1"),
                    modifiedTime = LocalDateTime.parse("2020-01-01T00:00:00.000")
                )),
                changefeed(section(
                    moduleCode = ModuleCode("ZQM1"),
                    sectionCode = SectionCode("ZQS2"),
                    modifiedTime = LocalDateTime.parse("2020-01-01T00:00:00.000")
                ))
            )
            val progress = listOf(
                changefeed(progress(
                    entityCode = EntityCode("ZS1"),
                    sectionCode = SectionCode("ZQS1"),
                    subsectionCode = SubsectionCode("ZQU1"),
                    answerCount = 1,
                    visibleQuestionCount = 3,
                    modifiedTime = LocalDateTime.parse("2020-02-01T00:00:00.000")
                )),
                changefeed(progress(
                    entityCode = EntityCode("ZS1"),
                    sectionCode = SectionCode("ZQS1"),
                    subsectionCode = SubsectionCode("ZQU2"),
                    answerCount = 2,
                    visibleQuestionCount = 5,
                    modifiedTime = LocalDateTime.parse("2020-02-01T00:00:00.000")
                )),
                changefeed(progress(
                    entityCode = EntityCode("ZS1"),
                    sectionCode = SectionCode("ZQS2"),
                    subsectionCode = SubsectionCode("ZQU3"),
                    answerCount = 4,
                    visibleQuestionCount = 4,
                    modifiedTime = LocalDateTime.parse("2020-02-01T00:00:00.000")
                )),
                changefeed(progress(
                    entityCode = EntityCode("ZS1"),
                    sectionCode = SectionCode("ZQS2"),
                    subsectionCode = SubsectionCode("ZQU4"),
                    answerCount = 2,
                    visibleQuestionCount = 3,
                    modifiedTime = LocalDateTime.parse("2020-02-01T00:00:00.000")
                ))
            )
            topology.advanceSaqSectionInputTopic().pipeValueList(sections)
            topology.advanceSaqProgressInputTopic().pipeValueList(progress)
            assertEquals(mapOf(
                (SiteCode("ZS1") to ModuleCode("ZQM1")) to Percentage(60L)
            ), topology.moduleProgressOutputTopic().readRecordsToList().toProgressPercentages())
        }
    }

    fun section(
        moduleCode: ModuleCode,
        sectionCode: SectionCode,
        modifiedTime: LocalDateTime = LocalDateTime.now(),
        deleted: Boolean = false
    ) = ModuleSectionChange(
        moduleCode,
        sectionCode,
        modifiedTime,
        deleted
    )

    fun progress(
        entityCode: EntityCode,
        sectionCode: SectionCode,
        subsectionCode: SubsectionCode,
        answerCount: Long,
        visibleQuestionCount: Long,
        modifiedTime: LocalDateTime = LocalDateTime.now(),
        deleted: Boolean = false
    ) = EntitySAQProgressChange(
        entityCode,
        sectionCode,
        subsectionCode,
        answerCount,
        visibleQuestionCount,
        modifiedTime,
        deleted
    )

    fun changefeed(after: EntitySAQProgressChange): EntitySAQProgressChangeWrapper = EntitySAQProgressChangeWrapper(after)

    fun changefeed(after: ModuleSectionChange): ModuleSectionChangeWrapper = ModuleSectionChangeWrapper(after)
}

private fun List<TestRecord<SiteSAQModuleKey, SiteSAQModuleProgress>>.toProgressPercentages(): Map<Pair<SiteCode, ModuleCode>, Percentage> =
    associate {
        (it.value.siteCode to it.value.moduleCode) to percentage(it.value.answerCount, it.value.visibleQuestionCount)
    }

fun percentage(n: Long, m: Long) =
    Percentage(if (m > 0L) n * 100 / m else 0)

data class Percentage(val value: Long)
