package com.example.stream

import EntityCode
import ModuleCode
import SectionCode
import SubsectionCode
import java.time.LocalDateTime
import java.util.*
import kotlin.random.Random

fun randomEntityCode() = EntityCode(UUID.randomUUID().toString())
fun randomModuleCode() = ModuleCode(UUID.randomUUID().toString())
fun randomSectionCode() = SectionCode(UUID.randomUUID().toString())
fun randomSubsectionCode() = SubsectionCode(UUID.randomUUID().toString())

fun randomCount(until: Long) = Random.nextInt(until.toInt()).toLong()
fun randomCount() = Random.nextInt(100).toLong()

fun randomProgressChange(
    entityCode: EntityCode = randomEntityCode(),
    sectionCode: SectionCode = randomSectionCode(),
    subsectionCode: SubsectionCode = randomSubsectionCode(),
    visibleQuestionCount: Long = randomCount(),
    answerCount: Long = randomCount(visibleQuestionCount + 1),
    modifiedTime: LocalDateTime = LocalDateTime.now(),
    deleted: Boolean = false
): EntitySAQProgressChange = EntitySAQProgressChange(
    entityCode = entityCode,
    sectionCode = sectionCode,
    subsectionCode = subsectionCode,
    answerCount = answerCount,
    visibleQuestionCount = visibleQuestionCount,
    modifiedTime = modifiedTime,
    deleted = deleted
)

fun randomModuleSectionChange(
    moduleCode: ModuleCode = randomModuleCode(),
    sectionCode: SectionCode = randomSectionCode(),
    modifiedTime: LocalDateTime = LocalDateTime.now(),
    deleted: Boolean = false
): ModuleSectionChange = ModuleSectionChange(
    moduleCode = moduleCode,
    sectionCode = sectionCode,
    modifiedTime = modifiedTime,
    deleted = deleted
)
