package com.example.stream

import org.junit.jupiter.api.Test

class CapacityPlanningTest {

    @Test
    fun `how many bytes for a section change value`() {
        println(Json.writeValueAsBytes(exampleSectionChange).size)
    }
}

// TODO: Does this cover all fields?
val exampleSectionChange = mapOf(
    "after" to mapOf(
        "module_code" to "ZQM296780520",
        "section_code" to "ZQS296780521",
        "section_name" to "Reporting Year",
        "section_description" to null,
        "version" to 1,
        "sort_index" to 5,
        "modified_by" to "ZU3200152",
        "modified_on" to "2017-08-30T16:42:24.119",
        "deleted" to false,
        "last_saq_progress_sychronized_on" to null,
        "saq_progress_synchronizing_status" to null,
        "sl_id" to 628440856593858565L,
        "sldeleted" to false,
        "slmodifieddatetime" to "2021-01-28T17:35:27.037"
    )
)
