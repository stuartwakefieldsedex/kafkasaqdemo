package com.example.stream

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class TestDescribeTopology {
    @Test
    fun `describe topology`() {
        val expected = javaClass.getResource("/topology")?.readText()?.trim()
        val actual = topology().describe().toString().trim()
        assertEquals(expected, actual)
    }
}
