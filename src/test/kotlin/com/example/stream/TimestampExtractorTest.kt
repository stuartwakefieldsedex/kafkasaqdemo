package com.example.stream

import org.apache.kafka.common.serialization.Serdes
import org.apache.kafka.streams.TestInputTopic
import org.apache.kafka.streams.TopologyTestDriver
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class TimestampExtractorTest {

    @Test
    fun `test timestamp extractor extracts time from modified on field`() {
        testTopology().use { topology ->
            topology.rawAdvanceSaqSectionInputTopic().pipeJson(
                mapOf(
                    "after" to mapOf(
                        "module_code" to "ZQM296780520",
                        "section_code" to "ZQS296780521",
                        "section_name" to "Reporting Year",
                        "section_description" to null,
                        "version" to 1,
                        "sort_index" to 5,
                        "modified_by" to "ZU3200152",
                        "modified_on" to "2017-08-30T16:42:24.119",
                        "deleted" to false,
                        "last_saq_progress_sychronized_on" to null,
                        "saq_progress_synchronizing_status" to null,
                        "sl_id" to 628440856593858565L,
                        "sldeleted" to false,
                        "slmodifieddatetime" to "2021-01-28T17:35:27.037"
                    )
                )
            )
            topology.rawAdvanceSaqProgressInputTopic().pipeJson(
                mapOf(
                    "after" to mapOf(
                        "entity_code" to "ZS1036083",
                        "section_code" to "ZQS296780521",
                        "subsection_code" to "ZQU409102271",
                        "answer_cnt" to 41,
                        "visible_question_cnt" to 42,
                        "modified_on" to "2020-10-07T07:39:43.833",
                        "deleted" to false,
                        "sl_id" to 628767291430764553L,
                        "slmodifieddatetime" to "2021-01-29T21:15:06.84"
                    )
                )
            )
            val result = topology.rawSubsectionProgressOutputTopic().readRecord()
            assertEquals(1602056383833L, result.timestamp())
        }
    }
}

fun TopologyTestDriver.rawAdvanceSaqProgressInputTopic() =
    createInputTopic(
        ADVANCE_SAQ_PROGRESS_TOPIC,
        Serdes.ByteArray().serializer(),
        Serdes.ByteArray().serializer()
    )

fun TopologyTestDriver.rawAdvanceSaqSectionInputTopic() =
    createInputTopic(
        ADVANCE_SAQ_SECTION_TOPIC,
        Serdes.ByteArray().serializer(),
        Serdes.ByteArray().serializer()
    )

fun TopologyTestDriver.rawSubsectionProgressOutputTopic() =
    createOutputTopic(
        SUBSECTION_PROGRESS_TOPIC,
        Serdes.ByteArray().deserializer(),
        Serdes.ByteArray().deserializer()
    )

fun TestInputTopic<*, ByteArray>.pipeJson(value: Any) =
    pipeInput(Json.writeValueAsBytes(value))
