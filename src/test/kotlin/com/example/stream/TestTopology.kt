package com.example.stream

import SiteSAQModuleKey
import SiteSAQModuleProgress
import SiteSAQProgress
import SiteSAQProgressKey
import SiteSAQSectionKey
import SiteSAQSectionProgress
import org.apache.kafka.common.serialization.Serdes
import org.apache.kafka.streams.StreamsConfig
import org.apache.kafka.streams.TopologyTestDriver

fun testTopology() =
    TopologyTestDriver(
        topology(),
        mapOf(
            StreamsConfig.BOOTSTRAP_SERVERS_CONFIG to "dummy:1234",
            StreamsConfig.APPLICATION_ID_CONFIG to "stuart-kafka-demo-test",
            StreamsConfig.DEFAULT_TIMESTAMP_EXTRACTOR_CLASS_CONFIG to CustomTimestampExtractor::class.java.canonicalName
        ).toProperties()
    )

fun TopologyTestDriver.subsectionProgressOutputTopic() =
    createOutputTopic(
        SUBSECTION_PROGRESS_TOPIC,
        Json.jsonSerde<SiteSAQProgressKey>().deserializer(),
        Json.jsonSerde<SiteSAQProgress>().deserializer()
    )

fun TopologyTestDriver.sectionProgressOutputTopic() =
    createOutputTopic(
        SECTION_PROGRESS_TOPIC,
        Json.jsonSerde<SiteSAQSectionKey>().deserializer(),
        Json.jsonSerde<SiteSAQSectionProgress>().deserializer()
    )

fun TopologyTestDriver.moduleProgressOutputTopic() =
    createOutputTopic(
        MODULE_PROGRESS_TOPIC,
        Json.jsonSerde<SiteSAQModuleKey>().deserializer(),
        Json.jsonSerde<SiteSAQModuleProgress>().deserializer()
    )

fun TopologyTestDriver.advanceSaqProgressInputTopic() =
    createInputTopic(
        ADVANCE_SAQ_PROGRESS_TOPIC,
        Serdes.ByteArray().serializer(),
        Json.jsonSerde<EntitySAQProgressChangeWrapper>().serializer()
    )

fun TopologyTestDriver.advanceSaqSectionInputTopic() =
    createInputTopic(
        ADVANCE_SAQ_SECTION_TOPIC,
        Serdes.ByteArray().serializer(),
        Json.jsonSerde<ModuleSectionChangeWrapper>().serializer()
    )
