package com.example.stream

import EntityCode
import ModuleCode
import SectionCode
import SubsectionCode
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class ResolveModuleCodeTest {

    @Test
    fun `resolve module code for progress`() {
        testTopology().use { topology ->

            // The progress change events from advance do not have the denormalized
            // module code in the message. We will need this to be able to aggregate
            // up progress by module code.
            val progressTopic = topology.advanceSaqProgressInputTopic()

            // The section topic contains the mapping we need to associate progress
            // via the section code to the module code.
            val sectionTopic = topology.advanceSaqSectionInputTopic()

            // Our output topic will have the enriched events containing the module code.
            val subsectionProgressTopic = topology.subsectionProgressOutputTopic()

            // The SAQ structure for our tests. We are using multiple modules here, even
            // though currently we are only interested in one. This is to verify the logic
            // works correctly.
            val saqStructure = listOf(
                ModuleCode("ZQM1") to listOf(
                    SectionCode("ZQS2") to listOf(
                        SubsectionCode("ZQU3")
                    )
                ),
                ModuleCode("ZQM2") to listOf(
                    SectionCode("ZQS1") to listOf(
                        SubsectionCode("ZQU2"),
                        SubsectionCode("ZQU5")
                    ),
                    SectionCode("ZQS3") to listOf(
                        SubsectionCode("ZQU1"),
                        SubsectionCode("ZQU4")
                    )
                )
            )

            // Progress is recorded against entities, we are filtering out anything
            // that isn't recorded against a site as this is all we are interested
            // in at the moment.
            val entityCode = EntityCode("ZS1")

            // The expected result contains the module codes, along with the section
            // codes and subsection codes that should already exists. We aren't testing
            // any aggregation or any other progress logic here.
            val expected = toSubsectionSectionModuleCodes(saqStructure)

            // Push our change events into the source topics. These would normally be
            // populated with a change feed (change data capture)
            sectionTopic.pipeValueList(toSectionModuleCodes(saqStructure).map {
                randomModuleSectionChange(
                    moduleCode = it.second,
                    sectionCode = it.first
                ).wrapped()
            })
            progressTopic.pipeValueList(expected.map {
                randomProgressChange(
                    entityCode = entityCode,
                    sectionCode = it.second,
                    subsectionCode = it.first
                ).wrapped()
            })

            // Read the results from our output topic
            val result = subsectionProgressTopic.readRecordsToList()

            // We are only interested in the resolved IDs at this point
            val actual = result.map { Triple(it.value.subsectionCode, it.value.sectionCode, it.value.moduleCode) }

            // Assert
            assertEquals(expected, actual)
        }
    }
}

fun toSubsectionSectionModuleCodes(saqStructure: List<Pair<ModuleCode, List<Pair<SectionCode, List<SubsectionCode>>>>>) =
    saqStructure.flatMap { module ->
        module.second.flatMap { section ->
            section.second.map { subsection ->
                Triple(subsection, section.first, module.first)
            }
        }
    }

fun toSectionModuleCodes(saqStructure: List<Pair<ModuleCode, List<Pair<SectionCode, List<SubsectionCode>>>>>) =
    saqStructure.flatMap { module ->
        module.second.map { section ->
            section.first to module.first
        }
    }

fun EntitySAQProgressChange.wrapped() = EntitySAQProgressChangeWrapper(this)

fun ModuleSectionChange.wrapped() = ModuleSectionChangeWrapper(this)
