package com.example.stream

import EntityCode
import ModuleCode
import SectionCode
import SiteCode
import SiteSAQSectionProgress
import SubsectionCode
import org.junit.jupiter.api.Test
import java.time.LocalDateTime
import kotlin.test.assertEquals

class SectionProgressTest {

    @Test
    fun `aggregates the progress counts to the section level`() {
        testTopology().use { topology ->
            topology.advanceSaqSectionInputTopic().pipeValueList(
                listOf(
                    ModuleSectionChangeWrapper(
                        after = ModuleSectionChange(
                            moduleCode = ModuleCode("ZQM1"),
                            sectionCode = SectionCode("ZQS1"),
                            modifiedTime = LocalDateTime.parse("2020-01-01T00:00:00.000"),
                            deleted = false
                        )
                    ),
                    ModuleSectionChangeWrapper(
                        after = ModuleSectionChange(
                            moduleCode = ModuleCode("ZQM1"),
                            sectionCode = SectionCode("ZQS2"),
                            modifiedTime = LocalDateTime.parse("2020-01-01T00:00:00.000"),
                            deleted = false
                        )
                    )
                )
            )
            topology.advanceSaqProgressInputTopic().pipeValueList(
                listOf(
                    EntitySAQProgressChangeWrapper(
                        after = EntitySAQProgressChange(
                            entityCode = EntityCode("ZS1"),
                            sectionCode = SectionCode("ZQS1"),
                            subsectionCode = SubsectionCode("ZQU1"),
                            answerCount = 1,
                            visibleQuestionCount = 3,
                            modifiedTime = LocalDateTime.parse("2020-02-01T00:00:00.000"),
                            deleted = false
                        )
                    ),
                    EntitySAQProgressChangeWrapper(
                        after = EntitySAQProgressChange(
                            entityCode = EntityCode("ZS1"),
                            sectionCode = SectionCode("ZQS1"),
                            subsectionCode = SubsectionCode("ZQU2"),
                            answerCount = 2,
                            visibleQuestionCount = 5,
                            modifiedTime = LocalDateTime.parse("2020-02-01T00:00:00.000"),
                            deleted = false
                        )
                    ),
                    EntitySAQProgressChangeWrapper(
                        after = EntitySAQProgressChange(
                            entityCode = EntityCode("ZS1"),
                            sectionCode = SectionCode("ZQS2"),
                            subsectionCode = SubsectionCode("ZQU3"),
                            answerCount = 4,
                            visibleQuestionCount = 4,
                            modifiedTime = LocalDateTime.parse("2020-02-01T00:00:00.000"),
                            deleted = false
                        )
                    ),
                    EntitySAQProgressChangeWrapper(
                        after = EntitySAQProgressChange(
                            entityCode = EntityCode("ZS1"),
                            sectionCode = SectionCode("ZQS2"),
                            subsectionCode = SubsectionCode("ZQU4"),
                            answerCount = 2,
                            visibleQuestionCount = 3,
                            modifiedTime = LocalDateTime.parse("2020-02-01T00:00:00.000"),
                            deleted = false
                        )
                    )
                )
            )
            assertEquals(
                listOf(
                    SiteSAQSectionProgress(
                        siteCode = SiteCode("ZS1"),
                        moduleCode = ModuleCode("ZQM1"),
                        sectionCode = SectionCode("ZQS1"),
                        answerCount = 1,
                        visibleQuestionCount = 3,
                        modifiedTime = LocalDateTime.parse("2020-02-01T00:00:00.000")
                    ),
                    SiteSAQSectionProgress(
                        siteCode = SiteCode("ZS1"),
                        moduleCode = ModuleCode("ZQM1"),
                        sectionCode = SectionCode("ZQS1"),
                        answerCount = 3,
                        visibleQuestionCount = 8,
                        modifiedTime = LocalDateTime.parse("2020-02-01T00:00:00.000")
                    ),
                    SiteSAQSectionProgress(
                        siteCode = SiteCode("ZS1"),
                        moduleCode = ModuleCode("ZQM1"),
                        sectionCode = SectionCode("ZQS2"),
                        answerCount = 4,
                        visibleQuestionCount = 4,
                        modifiedTime = LocalDateTime.parse("2020-02-01T00:00:00.000")
                    ),
                    SiteSAQSectionProgress(
                        siteCode = SiteCode("ZS1"),
                        moduleCode = ModuleCode("ZQM1"),
                        sectionCode = SectionCode("ZQS2"),
                        answerCount = 6,
                        visibleQuestionCount = 7,
                        modifiedTime = LocalDateTime.parse("2020-02-01T00:00:00.000")
                    )
                ),
                topology.sectionProgressOutputTopic().readRecordsToList().map { it.value }
            )
        }
    }

    @Test
    fun `aggregate the progress counts to the section level updates to the same subsection`() {
        testTopology().use { topology ->
            topology.advanceSaqSectionInputTopic().pipeValueList(
                listOf(
                    ModuleSectionChangeWrapper(
                        after = ModuleSectionChange(
                            moduleCode = ModuleCode("ZQM1"),
                            sectionCode = SectionCode("ZQS1"),
                            modifiedTime = LocalDateTime.parse("2020-01-01T00:00:00.000"),
                            deleted = false
                        )
                    ),
                    ModuleSectionChangeWrapper(
                        after = ModuleSectionChange(
                            moduleCode = ModuleCode("ZQM1"),
                            sectionCode = SectionCode("ZQS2"),
                            modifiedTime = LocalDateTime.parse("2020-01-01T00:00:00.000"),
                            deleted = false
                        )
                    )
                )
            )
            topology.advanceSaqProgressInputTopic().pipeValueList(
                listOf(
                    EntitySAQProgressChangeWrapper(
                        after = EntitySAQProgressChange(
                            entityCode = EntityCode("ZS1"),
                            sectionCode = SectionCode("ZQS1"),
                            subsectionCode = SubsectionCode("ZQU1"),
                            answerCount = 1,
                            visibleQuestionCount = 3,
                            modifiedTime = LocalDateTime.parse("2020-02-01T00:00:00.000"),
                            deleted = false
                        )
                    ),
                    EntitySAQProgressChangeWrapper(
                        after = EntitySAQProgressChange(
                            entityCode = EntityCode("ZS1"),
                            sectionCode = SectionCode("ZQS1"),
                            subsectionCode = SubsectionCode("ZQU2"),
                            answerCount = 2,
                            visibleQuestionCount = 5,
                            modifiedTime = LocalDateTime.parse("2020-02-01T00:00:00.000"),
                            deleted = false
                        )
                    ),
                    EntitySAQProgressChangeWrapper(
                        after = EntitySAQProgressChange(
                            entityCode = EntityCode("ZS1"),
                            sectionCode = SectionCode("ZQS2"),
                            subsectionCode = SubsectionCode("ZQU3"),
                            answerCount = 4,
                            visibleQuestionCount = 4,
                            modifiedTime = LocalDateTime.parse("2020-02-01T00:00:00.000"),
                            deleted = false
                        )
                    ),
                    EntitySAQProgressChangeWrapper(
                        after = EntitySAQProgressChange(
                            entityCode = EntityCode("ZS1"),
                            sectionCode = SectionCode("ZQS2"),
                            subsectionCode = SubsectionCode("ZQU4"),
                            answerCount = 2,
                            visibleQuestionCount = 3,
                            modifiedTime = LocalDateTime.parse("2020-02-01T00:00:00.000"),
                            deleted = false
                        )
                    ),
                    EntitySAQProgressChangeWrapper(
                        after = EntitySAQProgressChange(
                            entityCode = EntityCode("ZS1"),
                            sectionCode = SectionCode("ZQS1"),
                            subsectionCode = SubsectionCode("ZQU1"),
                            answerCount = 3,
                            visibleQuestionCount = 3,
                            modifiedTime = LocalDateTime.parse("2020-02-02T00:00:00.000"),
                            deleted = false
                        )
                    ),
                    EntitySAQProgressChangeWrapper(
                        after = EntitySAQProgressChange(
                            entityCode = EntityCode("ZS1"),
                            sectionCode = SectionCode("ZQS2"),
                            subsectionCode = SubsectionCode("ZQU4"),
                            answerCount = 3,
                            visibleQuestionCount = 3,
                            modifiedTime = LocalDateTime.parse("2020-02-02T00:00:00.000"),
                            deleted = false
                        )
                    )
                )
            )
            assertEquals(
                listOf(
                    SiteSAQSectionProgress(
                        siteCode = SiteCode("ZS1"),
                        moduleCode = ModuleCode("ZQM1"),
                        sectionCode = SectionCode("ZQS1"),
                        answerCount = 1,
                        visibleQuestionCount = 3,
                        modifiedTime = LocalDateTime.parse("2020-02-01T00:00:00.000")
                    ),
                    SiteSAQSectionProgress(
                        siteCode = SiteCode("ZS1"),
                        moduleCode = ModuleCode("ZQM1"),
                        sectionCode = SectionCode("ZQS1"),
                        answerCount = 3,
                        visibleQuestionCount = 8,
                        modifiedTime = LocalDateTime.parse("2020-02-01T00:00:00.000")
                    ),
                    SiteSAQSectionProgress(
                        siteCode = SiteCode("ZS1"),
                        moduleCode = ModuleCode("ZQM1"),
                        sectionCode = SectionCode("ZQS2"),
                        answerCount = 4,
                        visibleQuestionCount = 4,
                        modifiedTime = LocalDateTime.parse("2020-02-01T00:00:00.000")
                    ),
                    SiteSAQSectionProgress(
                        siteCode = SiteCode("ZS1"),
                        moduleCode = ModuleCode("ZQM1"),
                        sectionCode = SectionCode("ZQS2"),
                        answerCount = 6,
                        visibleQuestionCount = 7,
                        modifiedTime = LocalDateTime.parse("2020-02-01T00:00:00.000")
                    ),

                    // We seem to get these intermediate states where the reducer is
                    // subtracting a previous added value, we have managed to hide the reduced
                    // value from the output topic but this has resulted in additional complexity.
                    // Without doing this progress goes backwards momentarily before being
                    // updated forwards.
                    SiteSAQSectionProgress(
                        siteCode = SiteCode("ZS1"),
                        moduleCode = ModuleCode("ZQM1"),
                        sectionCode = SectionCode("ZQS1"),
                        answerCount = 3,
                        visibleQuestionCount = 8,
                        modifiedTime = LocalDateTime.parse("2020-02-01T00:00:00.000")
                    ),

                    SiteSAQSectionProgress(
                        siteCode = SiteCode("ZS1"),
                        moduleCode = ModuleCode("ZQM1"),
                        sectionCode = SectionCode("ZQS1"),
                        answerCount = 5,
                        visibleQuestionCount = 8,
                        modifiedTime = LocalDateTime.parse("2020-02-02T00:00:00.000")
                    ),

                    // Here is another one
                    SiteSAQSectionProgress(
                        siteCode = SiteCode("ZS1"),
                        moduleCode = ModuleCode("ZQM1"),
                        sectionCode = SectionCode("ZQS2"),
                        answerCount = 6,
                        visibleQuestionCount = 7,
                        modifiedTime = LocalDateTime.parse("2020-02-01T00:00:00.000")
                    ),

                    SiteSAQSectionProgress(
                        siteCode = SiteCode("ZS1"),
                        moduleCode = ModuleCode("ZQM1"),
                        sectionCode = SectionCode("ZQS2"),
                        answerCount = 7,
                        visibleQuestionCount = 7,
                        modifiedTime = LocalDateTime.parse("2020-02-02T00:00:00.000")
                    )
                ),
                topology.sectionProgressOutputTopic().readRecordsToList().map { it.value }
            )
        }
    }
}
