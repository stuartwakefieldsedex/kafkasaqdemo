package com.example.stream

import EntityCode
import ModuleCode
import SectionCode
import SiteCode
import SiteSAQProgress
import SiteSAQProgressKey
import SubsectionCode
import com.fasterxml.jackson.annotation.JsonProperty
import java.lang.IllegalArgumentException
import java.time.LocalDateTime

// Anti-corruption, mapping the input data

data class EntitySAQProgressChangeWrapper(val after: EntitySAQProgressChange)

data class EntitySAQProgressChange(
    @JsonProperty("entity_code") val entityCode: EntityCode,
    @JsonProperty("section_code") val sectionCode: SectionCode,
    @JsonProperty("subsection_code") val subsectionCode: SubsectionCode,
    @JsonProperty("answer_cnt") val answerCount: Long,
    @JsonProperty("visible_question_cnt") val visibleQuestionCount: Long,
    @JsonProperty("modified_on") val modifiedTime: LocalDateTime,
    val deleted: Boolean
)

fun EntitySAQProgressChange.toSiteSAQProgressChange() = SiteSAQProgressChange(
    siteCode = entityCode.asSite() ?: throw IllegalArgumentException("Cannot convert to SiteSAQProgressChange as entity is not a site"),
    sectionCode = sectionCode,
    subsectionCode = subsectionCode,
    answerCount = answerCount,
    visibleQuestionCount = visibleQuestionCount,
    modifiedTime = modifiedTime,
    deleted = deleted
)

data class SiteSAQProgressChange(
    val siteCode: SiteCode,
    val sectionCode: SectionCode,
    val subsectionCode: SubsectionCode,
    val answerCount: Long,
    val visibleQuestionCount: Long,
    val modifiedTime: LocalDateTime,
    val deleted: Boolean
)

fun SiteSAQProgressChange.withModule(module: ModuleSectionChange) = SiteSAQProgress(
    siteCode = siteCode,
    moduleCode = module.moduleCode,
    sectionCode = sectionCode,
    subsectionCode = subsectionCode,
    answerCount = answerCount,
    visibleQuestionCount = visibleQuestionCount,
    modifiedTime = modifiedTime
)

data class ModuleSectionChangeWrapper(val after: ModuleSectionChange)
data class ModuleSectionChange(
    @JsonProperty("module_code") val moduleCode: ModuleCode,
    @JsonProperty("section_code") val sectionCode: SectionCode,
    @JsonProperty("modified_on") val modifiedTime: LocalDateTime,
    val deleted: Boolean
)

data class EntitySAQProgressKey(
    val entityCode: EntityCode,
    val subsectionCode: SubsectionCode
)

fun EntitySAQProgressKey.toSiteSAQProgressKey() = SiteSAQProgressKey(
    siteCode = entityCode.asSite() ?: throw IllegalArgumentException("Cannot convert to SiteSAQProgressKey as entity is not a site"),
    subsectionCode = subsectionCode
)
