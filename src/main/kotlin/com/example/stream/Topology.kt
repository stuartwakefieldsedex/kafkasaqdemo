package com.example.stream

import SectionCode
import SiteSAQModuleKey
import SiteSAQModuleProgress
import SiteSAQProgress
import SiteSAQProgressKey
import SiteSAQSectionKey
import SiteSAQSectionProgress
import org.apache.kafka.common.serialization.Serde
import org.apache.kafka.common.serialization.Serdes
import org.apache.kafka.streams.KeyValue
import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.Topology
import org.apache.kafka.streams.kstream.*
import toSiteSAQModuleProgress
import toSiteSAQSectionProgress
import java.time.LocalDate
import java.time.LocalTime
import java.util.*

fun topology(): Topology {
    val builder = StreamsBuilder()

    val sections = builder.stream(SourceTopics.advanceSaqSection).advanceSaqSectionTable()
    // Will also need module, subsection and site module to filter all deleted progress records
    val progress = builder.stream(SourceTopics.advanceSaqProgress).advanceSiteSaqProgressTable()

    val subsectionProgress = progress.subsectionProgressTable(sections)
    val sectionProgress = subsectionProgress.sectionProgressTable()
    val moduleProgress = subsectionProgress.moduleProgressTable()

    subsectionProgress.toStream().to(SinkTopics.subsectionProgress)
    sectionProgress.toStream().to(SinkTopics.sectionProgress)
    moduleProgress.toStream().to(SinkTopics.moduleProgress)

    return builder.build()
}

// Output topics

data class KafkaTopicSchema<K, V>(val name: String, val keySerdes: Serde<K>, val valueSerdes: Serde<V>)

const val ADVANCE_SAQ_PROGRESS_TOPIC = "stuart-saq-cdc-snaplogic_advance_saq_progress"

const val ADVANCE_SAQ_SECTION_TOPIC = "stuart-saq-cdc-snaplogic_advance_saq_section"

const val SUBSECTION_PROGRESS_TOPIC = "stuart-saq-site-subsection-progress"

const val SECTION_PROGRESS_TOPIC = "stuart-saq-site-section-progress"

const val MODULE_PROGRESS_TOPIC = "stuart-saq-site-module-progress"

object SourceTopics {
    val advanceSaqProgress = KafkaTopicSchema(
        ADVANCE_SAQ_PROGRESS_TOPIC,
        Serdes.ByteArray(),
        Json.jsonSerde<EntitySAQProgressChangeWrapper>()
    )
    val advanceSaqSection =
        KafkaTopicSchema(ADVANCE_SAQ_SECTION_TOPIC, Serdes.ByteArray(), Json.jsonSerde<ModuleSectionChangeWrapper>())
}

object SinkTopics {
    val moduleProgress = KafkaTopicSchema(
        MODULE_PROGRESS_TOPIC,
        Json.jsonSerde<SiteSAQModuleKey>(),
        Json.jsonSerde<SiteSAQModuleProgress>()
    )
    val sectionProgress = KafkaTopicSchema(
        SECTION_PROGRESS_TOPIC,
        Json.jsonSerde<SiteSAQSectionKey>(),
        Json.jsonSerde<SiteSAQSectionProgress>()
    )
    val subsectionProgress = KafkaTopicSchema(
        SUBSECTION_PROGRESS_TOPIC,
        Json.jsonSerde<SiteSAQProgressKey>(),
        Json.jsonSerde<SiteSAQProgress>()
    )
}

// Aggregations

private fun KTable<SiteSAQProgressKey, SiteSAQProgress>.moduleProgressTable() =
    groupBy(
        { _, v ->
            KeyValue(
                SiteSAQModuleKey(
                    v.siteCode,
                    v.moduleCode
                ), v.toSiteSAQModuleProgress().toAggregator()
            )
        },
        Grouped.with(Json.jsonSerde<SiteSAQModuleKey>(), Json.jsonSerde<SiteSAQModuleProgressAggregator>())
    )
    .reduce({ acc, v -> acc + v }, { acc, v -> acc - v })
    .mapValues { _, v -> v.result }

fun SiteSAQModuleProgress.toAggregator(): SiteSAQModuleProgressAggregator =
    SiteSAQModuleProgressAggregator(this, null)

data class SiteSAQModuleProgressAggregator(val result: SiteSAQModuleProgress, val staged: SiteSAQModuleProgress?)

operator fun SiteSAQModuleProgressAggregator.plus(other: SiteSAQModuleProgressAggregator) =
    copy(result = (staged ?: result) + other.result, staged = null)

operator fun SiteSAQModuleProgressAggregator.minus(other: SiteSAQModuleProgressAggregator) =
    copy(staged = result - other.result)

operator fun SiteSAQModuleProgress.plus(other: SiteSAQModuleProgress) =
    copy(
        answerCount = answerCount + other.answerCount,
        visibleQuestionCount = visibleQuestionCount + other.visibleQuestionCount,
        modifiedTime = Collections.max(listOf(modifiedTime, other.modifiedTime))
    )

operator fun SiteSAQModuleProgress.minus(other: SiteSAQModuleProgress) =
    copy(
        answerCount = answerCount - other.answerCount,
        visibleQuestionCount = visibleQuestionCount - other.visibleQuestionCount,
        modifiedTime = Collections.max(listOf(modifiedTime, other.modifiedTime))
    )

private fun KTable<SiteSAQProgressKey, SiteSAQProgress>.sectionProgressTable() =
    groupBy(
        { _, v ->
            KeyValue(
                SiteSAQSectionKey(
                    v.siteCode,
                    v.sectionCode
                ), v.toSiteSAQSectionProgress().toAggregator()
            )
        },
        Grouped.with(Json.jsonSerde<SiteSAQSectionKey>(), Json.jsonSerde<SiteSAQSectionProgressAggregator>())
    )
    .reduce({ acc, v -> acc + v }, { acc, v -> acc - v })
    .mapValues { _, v -> v.result }

fun SiteSAQSectionProgress.toAggregator(): SiteSAQSectionProgressAggregator =
    SiteSAQSectionProgressAggregator(this, null)

data class SiteSAQSectionProgressAggregator(val result: SiteSAQSectionProgress, val staged: SiteSAQSectionProgress?)

operator fun SiteSAQSectionProgressAggregator.plus(other: SiteSAQSectionProgressAggregator) =
    copy(result = (staged ?: result) + other.result, staged = null)

operator fun SiteSAQSectionProgressAggregator.minus(other: SiteSAQSectionProgressAggregator) =
    copy(staged = result - other.result)

operator fun SiteSAQSectionProgress.plus(other: SiteSAQSectionProgress) =
    copy(
        answerCount = answerCount + other.answerCount,
        visibleQuestionCount = visibleQuestionCount + other.visibleQuestionCount,
        modifiedTime = Collections.max(listOf(modifiedTime, other.modifiedTime))
    )

operator fun SiteSAQSectionProgress.minus(other: SiteSAQSectionProgress) =
    copy(
        answerCount = answerCount - other.answerCount,
        visibleQuestionCount = visibleQuestionCount - other.visibleQuestionCount,
        modifiedTime = Collections.max(listOf(modifiedTime, other.modifiedTime))
    )

private fun KTable<SiteSAQProgressKey, SiteSAQProgressChange>.subsectionProgressTable(
    sections: KTable<SectionCode, ModuleSectionChange>?
) =
    join(
        sections,
        SiteSAQProgressChange::sectionCode,
        { v1, v2 -> v1.withModule(v2) },
        Materialized.with(Json.jsonSerde<SiteSAQProgressKey>(), Json.jsonSerde<SiteSAQProgress>())
    )

// Advance Topology

private fun KStream<ByteArray, ModuleSectionChangeWrapper>.advanceSaqSectionTable() =
    peek { _, v -> println("stuart-saq-cdc-snaplogic_advance_saq_section: $v") }
    .map { _, v -> KeyValue(v.after.sectionCode, v.after) }
    .filter { _, v -> !v.deleted }
    .repartition(Repartitioned.with(Json.jsonSerde<SectionCode>(), Json.jsonSerde<ModuleSectionChange>()))
    .peek { _, v -> println("Modules by section code: $v") }
    .toTable()

private fun KStream<ByteArray, EntitySAQProgressChangeWrapper>.advanceSiteSaqProgressTable() =
    peek { _, v -> println("stuart-saq-cdc-snaplogic_advance_saq_progress: $v") }
    .map { _, v -> KeyValue(EntitySAQProgressKey(v.after.entityCode, v.after.subsectionCode), v.after) }
    .filter { _, v -> !v.deleted && v.entityCode.isSite }
    .map { k, v -> KeyValue(k.toSiteSAQProgressKey(), v.toSiteSAQProgressChange()) }
    .repartition(Repartitioned.with(Json.jsonSerde<SiteSAQProgressKey>(), Json.jsonSerde<SiteSAQProgressChange>()))
    .toTable()

fun <K, V> KStream<K, V>.to(topic: KafkaTopicSchema<K, V>) =
    to(topic.name, Produced.with(topic.keySerdes, topic.valueSerdes))

fun <K, V> StreamsBuilder.stream(topic: KafkaTopicSchema<K, V>): KStream<K, V> =
    stream(topic.name, Consumed.with(topic.keySerdes, topic.valueSerdes))
