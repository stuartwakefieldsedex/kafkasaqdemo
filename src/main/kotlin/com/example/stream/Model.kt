import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonValue
import java.time.LocalDateTime

// TODO: Duplication here, progress class needed

data class SiteSAQSectionKey(
    val siteCode: SiteCode,
    val sectionCode: SectionCode
)

data class SiteSAQModuleKey(
    val siteCode: SiteCode,
    val moduleCode: ModuleCode
)

data class SiteSAQProgressKey(
    val siteCode: SiteCode,
    val subsectionCode: SubsectionCode
)

data class SiteSAQProgress(
    val siteCode: SiteCode,
    val moduleCode: ModuleCode,
    val sectionCode: SectionCode,
    val subsectionCode: SubsectionCode,

    val answerCount: Long,
    val visibleQuestionCount: Long,
    val modifiedTime: LocalDateTime
)

fun SiteSAQProgress.toSiteSAQSectionProgress() = SiteSAQSectionProgress(
    siteCode = siteCode,
    moduleCode = moduleCode,
    sectionCode = sectionCode,

    answerCount = answerCount,
    visibleQuestionCount = visibleQuestionCount,
    modifiedTime = modifiedTime
)

fun SiteSAQProgress.toSiteSAQModuleProgress() = SiteSAQModuleProgress(
    siteCode = siteCode,
    moduleCode = moduleCode,

    answerCount = answerCount,
    visibleQuestionCount = visibleQuestionCount,
    modifiedTime = modifiedTime
)

data class SiteSAQSectionProgress(
    val siteCode: SiteCode,
    val moduleCode: ModuleCode,
    val sectionCode: SectionCode,

    val answerCount: Long,
    val visibleQuestionCount: Long,
    val modifiedTime: LocalDateTime
)

data class SiteSAQModuleProgress(
    val siteCode: SiteCode,
    val moduleCode: ModuleCode,

    val answerCount: Long,
    val visibleQuestionCount: Long,
    val modifiedTime: LocalDateTime
)

data class EntityCode @JsonCreator(mode = JsonCreator.Mode.DELEGATING) constructor(@JsonValue val value: String) {
    val isSite = value.matches(Regex("^ZS.*$"))
    fun asSite(): SiteCode? = if (isSite) SiteCode(value) else null
}
data class SiteCode @JsonCreator(mode = JsonCreator.Mode.DELEGATING) constructor(@JsonValue val value: String)
data class ModuleCode @JsonCreator(mode = JsonCreator.Mode.DELEGATING) constructor(@JsonValue val value: String)
data class SectionCode @JsonCreator(mode = JsonCreator.Mode.DELEGATING) constructor(@JsonValue val value: String)
data class SubsectionCode @JsonCreator(mode = JsonCreator.Mode.DELEGATING) constructor(@JsonValue val value: String)
