package com.example.stream

import org.apache.kafka.streams.KafkaStreams
import org.apache.kafka.streams.Topology
import java.util.*

fun main() {
    val streams = topology().asKafkaStreams(Config().toKafkaStreamsProperties())
    streams.start()
    Runtime.getRuntime().addShutdownHook(Thread {
        streams.close()
    })
}

fun Topology.asKafkaStreams(props: Properties): KafkaStreams = KafkaStreams(
    this,
    props
)
