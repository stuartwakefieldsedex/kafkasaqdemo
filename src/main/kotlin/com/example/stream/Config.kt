package com.example.stream

import org.apache.kafka.streams.StreamsConfig
import java.time.Duration

const val DEFAULT_BOOTSTRAP_SERVERS = "localhost:9092"
const val DEFAULT_APPLICATION_ID = "stuart-kafka-demo-v0.1.0"
const val DEFAULT_COMMIT_INTERVAL_MS = 1000L

data class Config(
    val bootstrapServers: List<String> = DEFAULT_BOOTSTRAP_SERVERS.split(","),
    val applicationId: String = DEFAULT_APPLICATION_ID,
    val commitInterval: Duration = Duration.ofMillis(DEFAULT_COMMIT_INTERVAL_MS)
)

fun Config.toKafkaStreamsProperties() = mapOf(
    StreamsConfig.BOOTSTRAP_SERVERS_CONFIG to bootstrapServers.joinToString(),
    StreamsConfig.APPLICATION_ID_CONFIG to applicationId,
    StreamsConfig.DEFAULT_TIMESTAMP_EXTRACTOR_CLASS_CONFIG to CustomTimestampExtractor::class.java.canonicalName,
    StreamsConfig.COMMIT_INTERVAL_MS_CONFIG to commitInterval.toMillis().toString()
).toProperties()
