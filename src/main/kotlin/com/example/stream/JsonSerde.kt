package com.example.stream

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.apache.kafka.common.serialization.Serdes

inline fun <reified T> ObjectMapper.jsonSerde() = Serdes.WrapperSerde(
    { _, change -> writeValueAsBytes(change) },
    { _, data -> readValue(data, T::class.java) }
)

val Json: ObjectMapper = jacksonObjectMapper()
    .registerModule(JavaTimeModule())
    .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
