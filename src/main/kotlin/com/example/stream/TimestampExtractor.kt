package com.example.stream

import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.streams.processor.TimestampExtractor
import java.time.ZoneOffset

class CustomTimestampExtractor : TimestampExtractor {
    override fun extract(record: ConsumerRecord<Any, Any>, partitionTime: Long): Long {
        val modifiedTime = when (val value = record.value()) {
            is ModuleSectionChangeWrapper -> value.after.modifiedTime
            is EntitySAQProgressChangeWrapper -> value.after.modifiedTime
            else -> null
        }
        return modifiedTime
            ?.atOffset(ZoneOffset.UTC)
            ?.toInstant()
            ?.toEpochMilli() ?: record.timestamp()
    }
}
